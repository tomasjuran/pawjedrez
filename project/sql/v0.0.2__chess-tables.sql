-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: paw-tpi
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

DROP TABLE IF EXISTS `movimientos`;
DROP TABLE IF EXISTS `piezas`;
DROP TABLE IF EXISTS `partidas`;
DROP TABLE IF EXISTS `usuarios`;

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `partidas`
--

CREATE TABLE `partidas` (
  `id` int(11) NOT NULL,
  `fechahora` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `blancas` int(11) DEFAULT NULL,
  `negras` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `ganador` tinyint(4) DEFAULT NULL,
  `turno` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_partida_1_idx` (`blancas`),
  KEY `fk_partida_2_idx` (`negras`),
  CONSTRAINT `fk_partida_1` FOREIGN KEY (`blancas`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_partida_2` FOREIGN KEY (`negras`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `piezas`
--

CREATE TABLE `piezas` (
  `idpartida` int(11) NOT NULL,
  `idpieza` int(11) NOT NULL,
  `posicion` int(11) NOT NULL,
  `tipopieza` varchar(1) NOT NULL,
  `color` tinyint(4) NOT NULL,
  `movio` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idpartida`,`idpieza`),
  CONSTRAINT `fk_piezas_3` FOREIGN KEY (`idpartida`) REFERENCES `partidas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `movimientos`
--

CREATE TABLE `movimientos` (
  `idpartida` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `origen` int(11) NOT NULL,
  `destino` int(11) NOT NULL,
  PRIMARY KEY (`idpartida`,`orden`),
  CONSTRAINT `fk_movimiento_1` FOREIGN KEY (`idpartida`) REFERENCES `partidas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
