<?php

namespace core\database;

use PDO;
use PDOException;

class Connection {

	public static function make($config) {
		try {
	    return new PDO(
	        $config["CONNECTION"].";dbname=".$config["DBNAME"],
	        $config["USERNAME"],
	        $config["PASSWORD"],
	        $config["OPTIONS"]
	    );
		} catch (PDOException $e) {
	  	die($e->getMessage());
		}
  }
}

	
