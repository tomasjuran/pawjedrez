<?php

spl_autoload_register("autoload");

function autoload($classname) {
	// Try to find if namespace == real location
	$filename = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR .
		str_replace("\\", DIRECTORY_SEPARATOR, $classname) . ".php";
	if (file_exists($filename)) {
		include_once $filename;
	}
}

use core\App;
use core\database\Connection;
use core\oauth\GoogleAuthConfig;
use core\Logger;

/**
 * Load config
 */
App::bind("config", require "../config.php");

/**
 * Load database connection
 */
App::bind("database", Connection::make(App::get("config")["DATABASE"]));

/**
 * Load logger object
 */
App::bind("logger", Logger::getLogger());

/**
 * Google Auth
 */
App::bind("GoogleAuthConfig", GoogleAuthConfig::make(App::get("config")["GOOGLE_API"]));

App::bind("viewsDir", $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR
	. ".." . DIRECTORY_SEPARATOR
		. "app" . DIRECTORY_SEPARATOR 
			. "views" . DIRECTORY_SEPARATOR);
