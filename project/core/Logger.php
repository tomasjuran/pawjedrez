<?php

namespace core;

final class Logger {

	private $logfile = "../server.log";

	/**
	 * Implementa Singleton del Logger
	 * @return Logger La instance del Logger
	 */
	public static function getLogger() {
		static $instance = null;
		if (is_null($instance)) {
			$instance = new Logger();
		}
		return $instance;
	}

	/**
	 * Evitamos que pueda ser instanciado desde afuera
	 */
	private function __construct() {
	}

	public function info($string) {
		$this->writefile("[INFO] " . $string);
	}

	public function error($string) {
		$this->writefile("[ERROR] " . $string);
	}

	public function fatal($string) {
		$this->writefile("[FATAL] " . $string);
	}

	private function writefile($string) {
		$file = fopen($this->logfile, "a");
		fwrite($file, date("[Y-m-d H:i:s] ") . $string . PHP_EOL);
		fclose($file);
	}
}