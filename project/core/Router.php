<?php

namespace core;

use Exception;
use core\App;
use core\exceptions\ResourceNotFoundException;
use core\exceptions\MethodNotAllowedException;

class Router {

    private static $no_last_visited = ["log", "oauth", "index"];

    /**
     * Load the request's associated controller.
     *
     * @param array $request
     */
    public static function direct($request) {
        if (!$request->url_elements) {
            static::redirect("/home");
        } else {
            $controller_name = "app\\controllers\\" . ucfirst($request->url_elements[0]) . "Controller";
        }

        if (!class_exists($controller_name))
            throw new ResourceNotFoundException("No resource defined for this URI.");
            
        $controller = new $controller_name();
        $action = strtolower($request->verb);

        if (!method_exists($controller, $action))
            throw new MethodNotAllowedException("Method is not allowed for this URI.");
            
        // Save last visited
        $save_last_visited = true;
        $path = $_SERVER["PATH_INFO"];
        // Don't save if last visited was /login or /logout
        foreach (static::$no_last_visited as $string) {
            if (strpos($path, $string) !== false)
                $save_last_visited = false;
        }
        if ($save_last_visited) {
            $_SESSION["lastvisited"] = $path;   
        }
        
        $controller->$action($request);
    }

    /**
     * Redirect to a new page.
     *
     * @param  string $path
     */
    public static function redirect($path) {
      header("Location: " . $path, 303);
      App::get("logger")->info(logInfo("303 Redirect"));
      exit;
    }

    /**
     * Go back to last visited page.
     */
    public static function lastVisited() {
        if (isset($_SESSION["lastvisited"])) {
            static::redirect($_SESSION["lastvisited"]);
        } else {
            static::redirect("/home");
        }
    }
}