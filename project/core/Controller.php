<?php

namespace core;

use core\App;
use core\Router;

abstract class Controller {
	
	protected function checkUserLoggedIn() {
		if (isset($_SESSION["username"])) {
			App::bind("username", $_SESSION["username"]);
		} else {
			Router::redirect("/login");
		}
	}

	protected function view($name) {
		return App::get("viewsDir") . $name;
	}
}