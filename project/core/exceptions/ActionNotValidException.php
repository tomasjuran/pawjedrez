<?php

namespace core\exceptions;

use Exception;

class ActionNotValidException extends Exception {
}