<?php

namespace core\exceptions;

use Exception;

class ResourceNotFoundException extends Exception {
}