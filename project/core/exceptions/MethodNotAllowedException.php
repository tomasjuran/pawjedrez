<?php

namespace core\exceptions;

use Exception;

class MethodNotAllowedException extends Exception {
}