<?php

namespace core\oauth;

class GoogleAuthConfig {

	public static function make($config) {
		$client_secrets = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR
			. ".." . DIRECTORY_SEPARATOR
				. $config["AUTH_CONFIG"];
		return [
			"AUTH_CONFIG" => $client_secrets,
			"REDIRECT_URI" => $config["REDIRECT_URI"] . "/oauth/google"
		];
	}
}