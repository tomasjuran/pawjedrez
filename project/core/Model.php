<?php

namespace core;

use core\App;
use PDO;
use PDOException;

abstract class Model {

	protected $table = "";
	protected $keys = [];
	protected $fields = [];

	protected $pdo = null;
  public function __construct() {
    $this->pdo = App::get("database");
  }
	
	public function select($keysonly = false) {
		try {
			$where = $this->getWhere($keysonly);
			$query = $this->pdo->prepare("SELECT * FROM ".$this->table." $where");
			$query->execute($this->getValues());
			$results = $query->fetchAll(PDO::FETCH_ASSOC);
			return $this->ORM($results);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function insert() {
		try {
			$cols = $this->getCols();
			$vals = $this->getVals();
			$query = $this->pdo->prepare("INSERT INTO ".$this->table." ($cols) VALUES ($vals)");
			$query->execute($this->getValues());
			return true;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function update($keysonly = true) {
		try {
			$set = $this->getSet();
			$where = $this->getWhere($keysonly);
			$query = $this->pdo->prepare("UPDATE " .$this->table." $set $where");
			$query->execute(array_merge($this->getValues(),$this->getKeys()));
			return true;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	public function delete($keysonly = true) {
		try {
			$where = $this->getWhere($keysonly);
			$query = $this->pdo->prepare("DELETE FROM ".$this->table." $where");
			$query->execute($this->getKeys());
			return true;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	/**
	 * Devuelve la lista de objetos instanciados en base al resultado de una consulta select
	 */
	protected function ORM($results) {
		$objects = []; 
		foreach ($results as $result) {
			$object = new static();
			foreach ($result as $field => $value) {
				$object->$field = $value;
			}
			$objects[] = $object;
		}
		return $objects;
	}

	/**
	  * Devuelve el string "SET field1 = :field1, field2 = field2"
	  * @return type
	  */
	protected function getSet() {
		return $this->fieldIterator("SET ", ", ", false);
	}

	/**
	 * Devuelve el string "WHERE field1 = :field1 AND field2 = :field2".
	 * Si $keysonly es true, armará el where sólo con las keys.
	 * De lo contrario, armará el where sólo con los fields.
	 * @param boolean $keysonly Bandera para usar sólo las keys
	 * @return type
	 */
	protected function getWhere($keysonly) {
		return $this->fieldIterator("WHERE ", " AND ", $keysonly);
	}

	/**
	 * Arma un string iterando sobre los fields que contienen datos.
	 * Utilizado para armar WHERE y SET
	 * Si $keysonly es true, armará el string sólo con las keys.
	 * De lo contrario, armará el string sólo con los fields.
	 * @param string $first El string con el que debe empezar la sentencia
	 * @param string $separator El string separador de fields
	 * @param boolean $keysonly Bandera para usar sólo las keys
	 * @return string El string armado
	 */
	protected function fieldIterator($first, $separator, $keysonly) {
		if ($keysonly) {
			$fieldsIfSet = $this->getKeysIfSet();
		} else {
			$fieldsIfSet = $this->getFieldsIfSet();
		}
		$string = "";
		$b = true;
		foreach ($fieldsIfSet as $field) {
			if ($b) {
				$string = $first;
				$b = false;
			} else {
				$string .= $separator;
			}
			$string .= $field . " = :" . $field;
		}
		return $string;
	}

	/**
	 * Devuelve un Array con los nombres de los fields que contienen datos
	 * para las funciones getWhere y getSet
	 * @return Array
	 */
	protected function getFieldsIfSet() {
		$fieldsIfSet = [];
		foreach ($this->fields as $field) {
			if (isset($this->$field)) {
				$fieldsIfSet[] = $field;
			}
		}
		return $fieldsIfSet;
	}

	/**
	 * Devuelve un Array con los nombres de las keys que contienen datos
	 * para la función getWhere
	 * @return Array
	 */
	protected function getKeysIfSet() {
		$keysIfSet = [];
		foreach ($this->keys as $key) {
			if (isset($this->$key)) {
				$keysIfSet[] = $key;
			}
		}
		return $keysIfSet;
	}

	/**
	 * Devuelve los fields en un String separados por coma 
	 * @return String
	 */
	protected function getCols() {
		return implode(", ", $this->getFieldsIfSet());
	}

	/**
	 * Devuelve un String listo para ser usado en una query PDO
	 * ":field1, :field2, :field3"
	 * @return type
	 */
	protected function getVals() {
		return ":".implode(", :", $this->getFieldsIfSet());
	}

	/**
	 * Devuelve un array asociativo, tal que $data[":field" => field]
	 * @return type
	 */
	protected function getValues() {
		$data = [];
		foreach ($this->fields as $field) {
			if (isset($this->$field)) {
				$data[":".$field] = $this->$field;
			}
		}
		return $data;
	}

	/**
	 * Devuelve un array asociativo, tal que $data[":id" => id]
	 * @return type
	 */
	protected function getKeys() {
		$data = [];
		foreach ($this->keys as $key) {
				$data[":".$key] = $this->$key;
		}
		return $data;
	}
}