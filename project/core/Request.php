<?php 

namespace core;

class Request {
	public $verb = "GET";
  public $url_elements = [];
  public $parameters = [];

  public function __construct() {
      $this->verb = $_SERVER["REQUEST_METHOD"];
      if (isset($_SERVER["PATH_INFO"]))
        $this->url_elements = array_slice(explode("/", $_SERVER["PATH_INFO"]), 1);
      $this->parseIncomingParams();
  }

  private function parseIncomingParams() {
      $parameters = [];

      if ($_SERVER["REQUEST_METHOD"] == "GET") {
        // Get GET Query String
        if (isset($_SERVER["QUERY_STRING"])) {
            parse_str($_SERVER["QUERY_STRING"], $parameters);
        }
      } else {
        // Get PUT, POST or DELETE body
        if (isset($_SERVER["CONTENT_TYPE"])) {
          if ($_SERVER["CONTENT_TYPE"] == "application/x-www-form-urlencoded") {
            // Parameters input via form
            $parameters = $_POST; 
          } else {
            // Parameters input in json format
            $parameters = json_decode(file_get_contents("php://input"), true);
          }
        }
      }

      $this->parameters = $parameters;
  }

  public static function returnError($type) {
    $request = new Request();
    $request->verb = "GET";
    $request->url_elements = ["error", $type];
    $request->parameters = [];
    return $request;
  }
}