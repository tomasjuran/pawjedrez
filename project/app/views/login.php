<?php require "fragments/header.php" ?>

<div class="login-register-container">
	<ul>
		<li><button id="login-tab" class="tab tab-active" onclick="tabDisplay(LOGIN)">Ingresar</button></li><!--
	--><li><button id="register-tab" class="tab" onclick="tabDisplay(REGISTER)">Registrarse</button></li>
	</ul>
	<div class="login-container">
		<form action="/login" method="post" id="login-form" class="generic-form">
			<label for="login-username">Usuario</label>
			<input type="text" id="login-username" name="login-username" required autofocus
				<?php
					if (isset($_SESSION["login-username"]))
						echo 'value="' . $_SESSION["login-username"].'"';
					if (isset($errors["login"]))
						echo 'class="error-form"'; 
				?>
			>
			<label for="login-password">Contraseña</label>
			<input type="password" id="login-password" name="login-password" required
				<?php
					if (isset($errors["login"]))
						echo 'class="error-form"';
				?>
			>
			<input type="submit" name="login" value="Ingresar">
		</form>
		<form action="/login" method="post" id="register-form" class="generic-form">
			<label for="register-username">Usuario (letras y números)</label>
			<input type="text" id="register-username" name="register-username" required
				<?php
					if (isset($_SESSION["register-username"]))
						echo 'value="' . $_SESSION["register-username"] . '"';
					if (isset($errors["register-username"]))
						echo 'class="error-form"';
				?>
			>
			<label for="register-password">Contraseña (al menos 8 caracteres)</label>
			<input type="password" id="register-password" name="register-password" required
				<?php
					if (isset($errors["register-password"]))
						echo 'class="error-form"';
				?>
			>
			<label for="register-confirm-password">Repetir contraseña</label>
			<input type="password" id="register-confirm-password" name="register-confirm-password" required
				<?php
					if (isset($errors["register-confirm-password"]))
						echo 'class="error-form"';
				?>
			>
			<label for="register-email">Email</label>
			<input type="email" id="register-email" name="register-email" required
				<?php
					if (isset($_SESSION["register-email"])) 
						echo 'value="' . $_SESSION["register-email"] . '"';
					if (isset($errors["register-email"]))
						echo 'class="error-form"';
				?>
			>
			<input type="submit" name="register" value="Registrarse">
		</form>
		<div class="oauth-login-container">
			<p>Ingresar con</p>
			<a class="oauth-login generic-button" href="/oauth/google"><span class="oauth-login-logo google-logo"></span>Google</a>
		</div>
	</div>
</div>

<?php require "fragments/footer.php" ?>