<!DOCTYPE html>
<html lang="es-AR">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="TP Integrador - Ajedrez Online - Programación en Ambiente Web - Universidad Nacional de Luján - 2019">
	<meta name="author" content="Juran, Martín Tomás">
	<meta name="keywords" content="HTML, CSS, XML, JavaScript, ECMAScript, Apache, PHP, MySQL, Game, Chess, Ajedrez, Juego, Player, Multiplayer, Online">
	<title><?php if (isset($title)) echo $title; else echo "PAWjedrez - Ajedrez Online" ?></title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" media="only screen and (orientation: landscape) and (min-width: 768px)" href="/css/style-landscape.css">
	<link rel="shortcut icon" type="image/x-icon" href="/img/pieces/queenb.svg">
	<?php if (isset($javascript)) echo $javascript ?>
</head>
<body>
	<header>
		<nav>
			<ul class="nav-main">
				<li><a class="nav-link" href="/home">Inicio</a></li>
				<li><a class="nav-link" href="/match/new">Nueva partida</a></li>
			</ul>
			<ul class="nav-user">
				<?php 
					if (isset($_SESSION["username"])) {
						echo '<li><a class="nav-link" href="/user/'.$_SESSION["username"].'">'.$_SESSION["username"].'</a></li>';
						echo
							'<li><form action="/logout" method="post" id="logout-form">
								<button class="nav-button generic-button" type="submit" name="logout">Cerrar sesión</button>
							</form></li>';
					}
					else
						echo '<li><a class="nav-button generic-button" href="/login">Ingresar o Registrarse</a></li>';
				?>	
			</ul>
		</nav>
	</header>
	<main>
		<?php require "error-pop-up.php" ?>