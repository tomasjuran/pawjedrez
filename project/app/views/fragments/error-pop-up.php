<?php 

use core\App;
$errors = App::get("errors");
if (!empty($errors)) : ?>
	<div class="error-pop-up">
		<?php foreach ($errors as $error => $message) : ?>
			<p class="error-pop-up-message"><?= $message ?></p>
		<?php endforeach ?>
	</div>
<?php endif ?>