<?php require "fragments/header.php" ?>

<div id="alert-div" class="alert-div-inactive" onclick="ChessGame.removeAlert()"><p></p></div>

<div id="chess-board-div"></div>

<div class="match-sidebar">
	<div class="match-controls-container">
		<div class="match-status-container">
			<p id="match-opponent">Buscando un oponente...</p>
			<p id="match-status">La partida aun no ha comenzado</p>
		</div>
		<div class="match-concede-container">
			<button class="match-concede" onclick="ChessGame.concede()">Admitir derrota</button>	
		</div>
	</div>
	<div class="match-moves-container">
		<ol id="match-moves-list"></ol>
	</div>
</div>

<?php require "fragments/footer.php" ?>