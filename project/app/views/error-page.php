<?php require "fragments/header.php"; ?>

<section class="error-page">
	<h1><?= $errorHeader ?></h1>
	<p><?= $errorDetail ?></p>
</section>

<?php require "fragments/footer.php"; ?>