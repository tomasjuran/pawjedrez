<?php require "fragments/header.php" ?>

<div class="main-container user-profile-container">
	<section class="user-profile">
		<h2>Perfil de <?= $user->getNombre() ?></h2>
		<p>Correo electrónico: <?= $user->getEmail() ?></p>
		<p>Partidas jugadas: <?= $played ?></p>
		<p>Siendo blancas: <?= $white ?></p>
		<p>Siendo negras: <?= $black ?></p>
		<p>Partidas ganadas: <?= $won ?></p>
		<p>Porcentaje de victoria: <?= number_format($winper * 100, 2, ",", "") ?>%</p>
	</section>

	<section>
		<h2>Partidas jugadas</h2>
		<div class="scrollable">
			<table class="basic-table">
					<thead>
						<tr>
							<th scope="col">Id</th>
							<th scope="col">Blancas</th>
							<th scope="col">Negras</th>
							<th scope="col">Ganador</th>
							<th scope="col">Fecha</th>
							<th scope="col">Movimientos</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($finished as $match): ?>
							<tr class="alter-rows selectable clickable" onclick="window.location = '/match/<?= $match->getId() ?>'">
								<td><?= $match->getId() ?></td>
								<td><?= $match->getBlancas()->getNombre() ?></td>
								<td><?= $match->getNegras()->getNombre() ?></td>
								<td><?= $match->getWinnerName() ?></td>
								<td><?= $match->getFechahora() ?></td>
								<td><?= count($match->getMovimientos()) ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
	</section>
</div>

<?php require "fragments/footer.php" ?>