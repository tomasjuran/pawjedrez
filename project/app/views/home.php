<?php require "fragments/header.php" ?>

<div class="home">
	<section class="home-welcome">
		<h1>¡Te damos la bienvenida a PAWjedrez<?php if(isset($username)) echo ", " . $username ?>!</h1>
		<a class="home-play-button generic-button" href="/match/new">Nueva partida</a> 
	</section>
	<section class="home-matches">
		<h2>Unirse a una partida</h2>
		<div class="home-matches-container basic-container scrollable">
			<table class="basic-table">
				<thead>
					<tr>
						<th scope="col">Id</th>
						<th scope="col">Blancas</th>
						<th scope="col">Negras</th>
						<th scope="col" colspan="2">Links</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($matches as $match): ?>
					<tr class="alter-rows">
						<td class="home-matches-index"><?= $match->getId() ?></td>
						<td class="home-matches-user"><a class="bold-link" href="/user/<?= $match->getBlancas()->getNombre() ?>"><?= $match->getBlancas()->getNombre() ?></a></td>
						<?php
							$td = '<td class="home-matches-user">';
							$negras = $match->getNegras();
							if (isset($negras))
								$td .= '<a class="bold-link" href="/user/'.$negras->getNombre().'"">'.$negras->getNombre().'</a>';
							else
								$td .= '&lt;&lt;DISPONIBLE&gt;&gt;';
							$td .= '</td>';
							echo $td;

							$td = '<td><a class="bold-link" href="/match/'.$match->getId().'">Ver</a>';
							if (!isset($negras)) {
 								echo $td . '</td>';
								$td = '<td><a class="bold-link" href="/match/'.$match->getId().'/join">Unirse</a>';
							}
							$td .= '</td>';
							echo $td;
						?>
					</tr>
				<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</section>
</div>

<?php require "fragments/footer.php" ?>