<?php

namespace app\models;

use core\Model;

class Usuario extends Model {

	protected $id;
	protected $nombre;
	protected $email;
	protected $contrasena;

	public function __construct() {
		$this->table = "usuarios";
		$this->keys = ["id"];
		$this->fields = [
			"id",
			"nombre",
			"email",
			"contrasena",
			];
		parent::__construct();
	}

  public function insert() {
    $errors = [];
    
    if (!isset($this->nombre)) {
      $errors["username-not-set"] = "Se necesita ingresar un nombre de usuario.";
      $errors["username"] = "";
    } else {
      $errors = array_merge($errors, $this->verifyUsername());
    }

    if (!isset($this->contrasena)) {
      $errors["password-not-set"] = "Se necesita ingresar una contraseña.";
      $errors["password"] = "";
    } else {
      $errors = array_merge($errors, $this->verifyPassword());
    }

    if (!isset($this->email)) {
      $errors["email-not-set"] = "Se necesita ingresar un email.";
      $errors["email"] = "";
    } else {
      $errors = array_merge($errors, $this->verifyEmail());
    }

    if (!empty($errors)) return $errors;

    $this->setId($this->getMaxId() + 1);
    $this->setContrasena($this->createHash());
    parent::insert();
    return $errors;
  }

  public function update($keysonly = true) {
    $errors = [];
    
    if (!isset($this->id)) {
      $errors["id-not-set"] = "Internal error. Id needed.";
    }

    if (isset($this->nombre)) {
      $errors = array_merge($errors, $this->verifyUsername());
    }

    if (isset($this->contrasena)) {
      $errors = array_merge($errors, $this->verifyPassword());
    }

    if (isset($this->email)) {
      $errors = array_merge($errors, $this->verifyEmail());
    }

    if (!empty($errors)) return $errors;

    parent::update();
    return $errors;
  }

  private function verifyUsername() {
    $errors = [];
    if (!ctype_alnum($this->nombre)) {
        $errors["user-not-alphanumeric"] = "El nombre de usuario debe contener solo letras y números.";
    } else {
      $userModel = new Usuario();
      $userModel->setNombre($this->nombre);
      if ($userModel->select()) {
        $errors["user-exists"] = "El nombre de usuario ya existe.";
      }
    }
    if (!empty($errors)) $errors["username"] = "";
    return $errors;
  }

  private function verifyPassword() {
    $errors = [];
    if (strlen($this->contrasena) < 8) {
      $errors["password-too-short"] = "La contraseña debe tener al menos 8 caracteres.";
      $errors["password"] = "";
    }
    return $errors;
  }

  private function verifyEmail() {
    $errors = [];
    if (preg_match("/\S+@[a-zA-Z]+\.[a-zA-Z]{2,3}(?:\.[a-zA-Z]{2})?/", $this->email) != 1) {
      $errors["email-invalid"] = "La dirección de email no es válida.";
    } else {
      $userModel = new Usuario();
      $userModel->setEmail($this->email);
      if ($userModel->select()) {
        $errors["email-exists"] = "Ya existe un usuario registrado con la cuenta de correo.";
      }
    }
    if (!empty($errors)) $errors["email"] = "";
    return $errors;
  }

  public function verifyCredentials() {
    $user = static::findUsuarioByNombre($this->getNombre());
    if ($user) {
      if (password_verify($this->getContrasena(), $user->getContrasena())) {
        return true;
      } 
    }
    return false;
  }

  public static function findUsuarioById($id) {
    if (isset($id)) {
      $usuarioModel = new static();
      $usuarioModel->setId($id);
      $result = $usuarioModel->select();
      if (!empty($result))
        return $result[0];
    }
    return null;
  }

  public static function findUsuarioByNombre($nombre) {
    if (isset($nombre)) {
      if (ctype_alnum($nombre)) {
        $usuarioModel = new static();
        $usuarioModel->setNombre($nombre);
        $result = $usuarioModel->select();
        if (!empty($result))
          return $result[0];
      }
    }
    return null;
  }

  public function setAll($username, $password, $email) {
    $this->setNombre($username);
    $this->setContrasena($password);
    $this->setEmail($email);
  }

	public function getMaxId() {
		$query = $this->pdo->prepare("SELECT MAX(id) FROM usuarios");
    $query->execute();
    return $query->fetch()[0];
	}

  private function createHash() {
    return password_hash($this->getContrasena(), PASSWORD_BCRYPT);
  }

  /**
   * @return mixed
   */
  public function getId()
  {
      return $this->id;
  }

  /**
   * @param mixed $id
   *
   * @return self
   */
  public function setId($id)
  {
      $this->id = $id;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getNombre()
  {
      return $this->nombre;
  }

  /**
   * @param mixed $nombre
   *
   * @return self
   */
  public function setNombre($nombre)
  {
      $this->nombre = $nombre;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getEmail()
  {
      return $this->email;
  }

  /**
   * @param mixed $email
   *
   * @return self
   */
  public function setEmail($email)
  {
      $this->email = $email;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getContrasena()
  {
      return $this->contrasena;
  }

  /**
   * @param mixed $contrasena
   *
   * @return self
   */
  public function setContrasena($contrasena)
  {
      $this->contrasena = $contrasena;

      return $this;
  }

}