<?php

namespace app\models;

use Exception;
use PDO;
use core\exceptions\MoveNotValidException;
use core\exceptions\ActionNotValidException;
use core\Model;
use app\models\Usuario;
use app\models\Pieza;
use app\models\Movimiento;

class Partida extends Model {

	const PREPARING = 0; 
	const PLAYING = 1;
	const FINISHED = 2;

	const NOT_VALID = -1;
	const NORMAL_MOVE = 0;
	const DOUBLE_STEP = 1;
	const CAPTURE = 2;
	const EN_PASSANT = 3;

	const WHITE = Pieza::WHITE;
	const BLACK = Pieza::BLACK;

	protected $id;
	protected $fechahora;
	protected $blancas;
	private $blancasUser;
	protected $negras;
	private $negrasUser;
	protected $estado;
	protected $ganador;
	protected $turno;

	function __construct() {
		$this->table = "partidas";
		$this->keys = ["id"];
		$this->fields = [
			"id",
			"fechahora",
			"blancas",
			"negras",
			"estado",
			"ganador",
			"turno"
			];
		parent::__construct();
	}

	/*
	 * Game
	 */

	public function createMatch($username) {
		$user = $this->getUsuarioByNombre($username);
		if ($user) {
			$firstrow = ["R", "N", "B", "Q", "K", "B", "N", "R"];
			$this->id = $this->getMaxId() + 1;
			$this->fechahora = date("Y-m-d H:i:s");
			$this->estado = static::PREPARING;
			$this->turno = Pieza::WHITE;
			$this->setBlancas($user);
			$this->insert();
			for ($i = 0; $i < 8; $i++) {
				$this->createPiece($i, $i, $firstrow[$i], Pieza::WHITE);
				$this->createPiece($i+8, $i+8, "P", Pieza::WHITE);
				$this->createPiece($i+48, $i+48, "P", Pieza::BLACK);
				$this->createPiece($i+56, $i+56, $firstrow[$i], Pieza::BLACK);
			}
			return true;
		}
		return false;
	}

	private function createPiece($id, $pos, $type, $colour) {
		$piece = new Pieza();
		$piece->setIdpartida($this->id);
		$piece->setIdpieza($id);
		$piece->setPosicion($pos);
		$piece->setTipopieza($type);
		$piece->setColor($colour);
		$piece->setMovio(0);
		$piece->insert();
	}

	private function persistMove($orig, $dest) {
		$selectPieza = $this->getPiezas($dest);
		if (!empty($selectPieza)) {
			$selectPieza[0]->delete();
		}
		$piezaOrigen = $this->getPiezas($orig)[0];
		$piezaOrigen->setPosicion($dest);
		$piezaOrigen->setMovio(1);
		$piezaOrigen->update();
		// Castling
		if ($piezaOrigen->getTipopieza() == "K" && abs($dest - $orig) == 2) {
			if ($dest > $orig) {
				// Kingside
				$rook = $this->getPiezas($dest+1)[0];
				$rook->setPosicion($dest-1);
			} else {
				// Queenside
				$rook = $this->getPiezas($dest-2)[0];
				$rook->setPosicion($dest+1);
			}
			$rook->setMovio(1);
			$rook->update();
		}
		// En passant
		if ($piezaOrigen->getTipopieza() == "P" && (($dest - $orig) % 8 != 0)) {
			if ($this->enPassant($piezaOrigen, $dest)) {
				$colourMultiplier = $piezaOrigen->getColor() == Pieza::WHITE ? 1 : -1;
				$opponentPawn = $this->getPiezas($dest - 8 * $colourMultiplier);
				$opponentPawn[0]->delete();
			}
		}

		$movimiento = new Movimiento();
		$movimiento->setIdpartida($this->id);
		$movimiento->setOrden($this->countMovimientos());
		$movimiento->setOrigen($orig);
		$movimiento->setDestino($dest);
		$movimiento->insert();
	}

	public function joinMatch($username) {
		if ($this->getEstado() == static::PREPARING) {
			$user = $this->getUsuarioByNombre($username);
			if ($user) {
				$this->setNegras($user);
				$this->setEstado(static::PLAYING);
				$this->update();
				return true;
			}
		}
		return false;
	}

	private function nextTurn() {
		if (!$this->isCheckMate()) {
			if ($this->turno == Pieza::WHITE)
				$this->turno = Pieza::BLACK;
			else
				$this->turno = Pieza::WHITE;
		}
		$this->update();
	}

	private function isCheckMate() {
		// Check if there's only one King left
		$piezaModel = new Pieza();
		$piezaModel->setIdpartida($this->id);
		$piezaModel->setTipopieza("K");
		$select = $piezaModel->select();
		if (sizeof($select) == 1) {
			$this->setGanador($select[0]->getColor());
			$this->setEstado(static::FINISHED);
			return true;
		}
		return false;
	}

	// Admit defeat, concede the game
	public function concede($username) {
		if ($this->estado != static::PLAYING) 
			throw new ActionNotValidException("La partida no está en juego.");
		
		$winner = -1;
		$blancas = $this->getBlancas();
		if ($username != $blancas->getNombre()) {
			$negras = $this->getNegras();
			if ($username != $negras->getNombre())
				throw new ActionNotValidException("El usuario ".$username." no forma parte de esta partida.");
			else
				$winner = Pieza::WHITE;
		} else {
			$winner = Pieza::BLACK;
		}

		$this->setGanador($winner);
		$this->setEstado(static::FINISHED);
		$this->update();
		return true;
	}

	public function makeMove($username, $orig, $dest) {
		try {
			if ($this->isMatchInProgress()) {
				if ($this->isUserValid($username)) {
					if ($this->isUsersPiece($orig)) {
						if ($this->isMoveValid($orig, $dest, $this->turno)) {
							$this->persistMove($orig, $dest);
	
							$this->nextTurn();
						}
					}
				}			
			}
		} catch (Exception $e) {
			throw new MoveNotValidException($e->getMessage());
		}
	}

	private function isMatchInProgress() {
		switch ($this->getEstado()) {
			case static::PREPARING:
				throw new Exception("La partida aun no comienza.");
			case static::FINISHED:
				throw new Exception("La partida ya finalizó.");
			case static::PLAYING:
				return true;
			default:
				throw new Exception("Partida en un estado inválido.");
		}
	}

	private function isUserValid($username) {
		$user = $this->getUsuarioByNombre($username);
		if ($user) {
			if ($this->turno == Pieza::WHITE) {
				$userTurn = $this->getBlancas();
			} else {
				$userTurn = $this->getNegras();
			}
			// Only user in turn can move
			if ($userTurn->getId() == $user->getId()) {
				return true;
			}
		} else {
			throw new Exception("El usuario ".$username." no existe.");
		}
		throw new Exception("No es tu turno.");
	}

	private function isUsersPiece($orig) {
		$select = $this->getPiezas($orig);
		if (empty($select))
			throw new Exception("El casillero seleccionado no tiene una pieza.");

		$origPiece = $select[0];
		if ($origPiece->getColor() != $this->turno)
			throw new Exception("La pieza no le corresponde al jugador de turno.");

		return true;
	}

	public function isMoveValid($orig, $dest, $colour) {
		$defaultError = "Jugada inválida.";
		$origPiece = $this->getPiezas($orig)[0];
		$tipo = $origPiece->getTipopieza();
		
		$pawnMove = static::NORMAL_MOVE;
		// Valid moves
		if (!$origPiece->isMoveValid($orig, $dest, $colour)) {
			// If move is invalid, try for special moves
			switch ($tipo) {
				case "P":
					$pawnMove = $this->pawnSpecialMoves($origPiece, $dest, $colour);
					if ($pawnMove == static::NOT_VALID)
						throw new Exception($defaultError." El peón no puede moverse de esa manera.");
					break;
				case "K":
					if (!$this->castling($origPiece, $dest))
						throw new Exception($defaultError." El rey no puede moverse de esa manera.");
					break;
				default:
					// Other pieces don't have special moves. Move is invalid
					throw new Exception($defaultError." La pieza no puede moverse de esa manera.");
			}
		}

		// Detect collision
		$select = $this->getPiezas($dest);
		if (!empty($select)) {
			if ($select[0]->getColor() == $colour)
				throw new Exception($defaultError." No puedes moverte sobre tu propia pieza.");
			// Pawn can't capture with a normal move or double step
			if ($tipo == "P" && ($pawnMove == static::NORMAL_MOVE || $pawnMove == static::DOUBLE_STEP))
				throw new Exception($defaultError." El peón no puede capturar así.");
		}

		if (Pieza::TIPOS_PIEZAS[$tipo]["multimove"])
			// Check all squares up to dest
			if (!$this->checkMultimove($origPiece, $dest))
				throw new Exception($defaultError." Hay una pieza en el camino.");

		// Validate that king isn't on Check

		return true;
	}

	private function pawnSpecialMoves($pawn, $dest) {
		$colourMultiplier = $pawn->getColor() == Pieza::WHITE ? 1 : -1;
		$distance = ($dest - $pawn->getPosicion()) * $colourMultiplier;
		// Double step
		if (!$pawn->getMovio() && $distance == 16)
			// Do not jump over a piece
			if (empty($this->getPiezas($pawn->getPosicion() + 8 * $colourMultiplier)))
				return static::DOUBLE_STEP;
		// Capture
		if ($distance == 7 || $distance == 9) {
			$select = $this->getPiezas($dest);
			if (!empty($select))
				// Normal capture
				return static::CAPTURE;
			else 
				// Check for en passant capture
				if ($this->enPassant($pawn, $dest))
					return static::EN_PASSANT;
		}
		return static::NOT_VALID;
	}

	private function castling($king, $dest) {
		if ($king->getMovio())
			return false;
		
		if ($dest > $king->getPosicion()) {
			// Kingside
			$rookPos = $dest + 1;
			$distance = 1;
		} else {
			// Queenside
			$rookPos = $dest - 2;
			$distance = -1;
		}
		
		$select = $this->getPiezas($rookPos);
		if (empty($select))
			// No rook found
			return false;
		else if ($select[0]->getMovio())
			// Rook has already moved
			return false;

		$valid = true;
		$pos = $king->getPosicion() + $distance;
		// Check if spaces are unoccupied
		while ($valid && $pos != $rookPos) {
			if (!empty($this->getPiezas($pos)))
				$valid = false;
			$pos += $distance;
		}
		return $valid;
	}

	// Check for collisions on multimove pieces
	private function checkMultimove($piece, $dest) {
		$piecePos = intval($piece->getPosicion());
		$origX = $piecePos % 8;
		$origY = intdiv($piecePos, 8);
		$destX = $dest % 8;
		$destY = intdiv($dest, 8);
		
		$difX = $destX - $origX;
		$difY = $destY - $origY;
		
		$iterations = max(abs($difX), abs($difY));
		
		// Returns the direction (-1, 0, 1)
		$difX = $difX <=> 0;
		$difY = $difY <=> 0;

		for ($i = 1; $i < $iterations; $i++) {
			$pos = $piecePos + $difX * $i + $difY * $i * 8;
			$select = $this->getPiezas($pos);
			if (!empty($select)) {
				return false;
			}
		}

		return true;
	}

	private function enPassant($pawn, $dest) {
		$opponentColourMultiplier = $pawn->getColor() == Pieza::WHITE ? -1 : 1;
		$lastMove = $this->getMovimientos($this->countMovimientos() - 1)[0];
		$opponentPiece = $this->getPiezas($lastMove->getDestino())[0];
		// Last move was opponent's pawn
		if ($opponentPiece->getTipopieza() == "P") {
			// Last move was double step
			if (abs($lastMove->getOrigen() - $lastMove->getDestino()) == 16)
				// Moving to the square the pawn passed
				if (($lastMove->getOrigen() + 8 * $opponentColourMultiplier) == $dest)
					return true;
		}
		return false;
	}

	// Validates if a position can be attacked by an opponent's piece
	private function isInCheck($pos, $colour) {
		return false;
	}

	/* 
	 * Services
	 */

	public function getMaxId() {
		$query = $this->pdo->prepare("SELECT MAX(id) FROM ".$this->table);
    $query->execute();
    return $query->fetch()[0];
	}

	public function findPreparingMatches() {
		$model = new Partida();
		$model->setEstado(static::PREPARING);
		return $model->select();
	}

	public function findNotFinishedMatches() {
		$query = $this->pdo->query("SELECT * FROM ".$this->table." WHERE estado <> ".static::FINISHED);
		$results = $query->fetchAll(PDO::FETCH_ASSOC);
		return $this->ORM($results);
	}

	public static function findMatchByNombreUsuario($username) {
		$matches = [];
		$user = Usuario::findUsuarioByNombre($username);
		if ($user) {
			$partidaModel = new Partida();
			$partidaModel->setBlancas($user);
			$matches = $partidaModel->select();
			$partidaModel = new Partida();
			$partidaModel->setNegras($user);
			$matches = array_merge($matches, $partidaModel->select());
		}
		return $matches;
	}

	public function getOpponent($userColour) {
		if ($userColour == Pieza::WHITE)
			return $this->getNegras();
		else
			return $this->getBlancas();
	}

	public function getUserColour($username) {
		$color = -1; 
		$user = $this->getUsuarioByNombre($username);
		if ($user) {
			if (isset($this->blancas) && $user->getId() == $this->blancas)
				$color = Pieza::WHITE;
			else if (isset($this->negras) && $user->getId() == $this->negras)
				$color = Pieza::BLACK;
		}
		return $color;
	}

  private function getUsuarioById($id) {
  	return Usuario::findUsuarioById($id);
  }

  private function getUsuarioByNombre($nombre) {
  	return Usuario::findUsuarioByNombre($nombre);
  }

  public function getWinnerName() {
  	if (isset($this->ganador))
  		if ($this->ganador == static::WHITE)
  			return "Blancas";
  		else if ($this->ganador == static::BLACK)
  			return "Negras";
  	return "Nadie";
  }

	public function getPiezas($pos = null) {
		$piezaModel = new Pieza();
		if (isset($pos))
			$piezaModel->setPosicion($pos);
		$piezaModel->setIdpartida($this->id);
		return $piezaModel->select();
	}

	public function getRemainingMoves($order) {
		$movimientoModel = new Movimiento();
		if (!isset($order))
			$order = 0;
		return $movimientoModel->findMovimientoByIdpartidaGreaterThanOrden($this->id, $order);
	}

	public function getMovimientos($order = null) {
		$movimientoModel = new Movimiento();
		if (isset($order))
			$movimientoModel->setOrden($order);
		$movimientoModel->setIdpartida($this->id);
		return $movimientoModel->select();
	}

	public function countMovimientos() {
		$movimientoModel = new Movimiento();
		return $movimientoModel->countMovimientoByIdpartida($this->id);
	}

	/*
	 * CRUD
	 */

  public function getBlancas() {
  	if (!$this->blancasUser) {
  		$this->blancasUser = $this->getUsuarioById($this->blancas);
  	}
  	return $this->blancasUser;
  }

  public function setBlancas($blancas) {
  	$this->blancasUser = $blancas;
  	if ($blancas)
  		$this->blancas = $blancas->getId();
  	else
  		$this->blancas = null;
  }

  public function getNegras() {
  	if (!$this->negrasUser) {
  		$this->negrasUser = $this->getUsuarioById($this->negras);
  	}
  	return $this->negrasUser;
  }

  public function setNegras($negras) {
  	$this->negrasUser = $negras;
  	if ($negras)
  		$this->negras = $negras->getId();
  	else
  		$this->negras = null;
  }

  /**
   * @return mixed
   */
  public function getId()
  {
      return $this->id;
  }

  /**
   * @param mixed $id
   *
   * @return self
   */
  public function setId($id)
  {
      $this->id = $id;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getFechahora()
  {
      return $this->fechahora;
  }

  /**
   * @param mixed $fechahora
   *
   * @return self
   */
  public function setFechahora($fechahora)
  {
      $this->fechahora = $fechahora;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getEstado()
  {
      return $this->estado;
  }

  /**
   * @param mixed $estado
   *
   * @return self
   */
  public function setEstado($estado)
  {
      $this->estado = $estado;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getGanador()
  {
      return $this->ganador;
  }

  /**
   * @param mixed $ganador
   *
   * @return self
   */
  public function setGanador($ganador)
  {
      $this->ganador = $ganador;

      return $this;
  }

    /**
     * @return mixed
     */
    public function getTurno()
    {
        return $this->turno;
    }

    /**
     * @param mixed $turno
     *
     * @return self
     */
    public function setTurno($turno)
    {
        $this->turno = $turno;

        return $this;
    }
}