<?php

namespace app\models;

use core\Model;

class Pieza extends Model {

  const WHITE = 0;
  const BLACK = 1;

	const TIPOS_PIEZAS = [
		"P" => ["desc" => "pawn", "mov" => [8], "multimove" => false],
		"R" => ["desc" => "rook", "mov" => [1, 8], "multimove" => true],
		"N" => ["desc" => "knight", "mov" => [6, 10, 15, 17], "multimove" => false],
		"B" => ["desc" => "bishop", "mov" => [7, 9], "multimove" => true],
		"Q" => ["desc" => "queen", "mov" => [1, 8, 7, 9], "multimove" => true],
		"K" => ["desc" => "king", "mov" => [1, 8, 7, 9], "multimove" => false]
	];

	protected $idpartida;
	protected $idpieza;
	protected $posicion;
	protected $tipopieza;
	protected $color;
	protected $movio;

	function __construct() {
		$this->table = "piezas";
		$this->keys = ["idpartida", "idpieza"];
		$this->fields = [
			"idpartida",
			"idpieza",
			"posicion",
			"tipopieza",
			"color",
			"movio"
			];
    parent::__construct();  
	}

	public function isMoveValid($orig, $dest, $colour) {
		$valid = false;
    $colourMultiplier = $colour == static::WHITE ? 1 : -1;
    $distance = ($dest - $orig) * $colourMultiplier;
		$tipo = static::TIPOS_PIEZAS[$this->tipopieza];
		if ($tipo["multimove"]) {
			foreach($tipo["mov"] as $mov) {
				if ($distance % $mov == 0) {
          // Special case for Rook and Queen: DO NOT GO PAST THIS POINT IF YOU'RE ALLERGIC TO MATH
          if ($mov == 1)
            // If orig and dest aren't on the same row
            if ((floor($orig / 8) - floor($dest / 8)) != 0)
              continue;

          $valid = true;
          break;
				}
			}
		} else {
			foreach($tipo["mov"] as $mov) {
				if (abs($distance) == $mov) {
          // Special case for Pawn, whom cannot move backwards
          if ($this->tipopieza == "P" && $distance != $mov)
            continue;

					$valid = true;
					break;
				}
			}
		}
		return $valid;
	}

  public function findAllByIdpartidaOrderByPosicion($idpartida) {
    if (isset($this->idpartida)) {
      $query = $this->pdo->prepare("SELECT * FROM ".$this->table." WHERE idpartida = :idpartida ORDER BY posicion");
      return $query->execute(["idpartida" => $idpartida]);
    }
    return [];
  }

  /**
   * @return mixed
   */
  public function getIdpartida()
  {
      return $this->idpartida;
  }

  /**
   * @param mixed $idpartida
   *
   * @return self
   */
  public function setIdpartida($idpartida)
  {
      $this->idpartida = $idpartida;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getIdpieza()
  {
      return $this->idpieza;
  }

  /**
   * @param mixed $idpieza
   *
   * @return self
   */
  public function setIdpieza($idpieza)
  {
      $this->idpieza = $idpieza;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getPosicion()
  {
      return $this->posicion;
  }

  /**
   * @param mixed $posicion
   *
   * @return self
   */
  public function setPosicion($posicion)
  {
      $this->posicion = $posicion;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getTipopieza()
  {
      return $this->tipopieza;
  }

  /**
   * @param mixed $tipopieza
   *
   * @return self
   */
  public function setTipopieza($tipopieza)
  {
      $this->tipopieza = $tipopieza;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getColor()
  {
      return $this->color;
  }

  /**
   * @param mixed $color
   *
   * @return self
   */
  public function setColor($color)
  {
      $this->color = $color;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getMovio()
  {
      return $this->movio;
  }

  /**
   * @param mixed $movio
   *
   * @return self
   */
  public function setMovio($movio)
  {
      $this->movio = $movio;

      return $this;
  }
}