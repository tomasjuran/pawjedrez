<?php

namespace app\models;

use core\Model;
use PDO;

class Movimiento extends Model {

	protected $idpartida;
	protected $orden;
	protected $origen;
	protected $destino;

	function __construct() {
		$this->table = "movimientos";
		$this->keys = ["idpartida", "orden"];
		$this->fields = [
			"idpartida",
			"orden",
			"origen",
			"destino"
			];
        parent::__construct();
	}

    public function countMovimientoByIdpartida($idpartida) {
        $query = $this->pdo->prepare("SELECT COUNT(*) FROM ".$this->table." WHERE idpartida = :idpartida");
        $query->execute(["idpartida" => $idpartida]);
        return $query->fetchColumn();
    }

    public function findMovimientoByIdpartidaGreaterThanOrden($idpartida, $orden) {
        $query = $this->pdo->prepare("SELECT * FROM ".$this->table." WHERE idpartida = :idpartida AND orden > :orden");
        $query->execute(["idpartida" => $idpartida, "orden" => $orden]);
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        return $this->ORM($results);
    }

    /**
     * @return mixed
     */
    public function getIdpartida()
    {
        return $this->idpartida;
    }

    /**
     * @param mixed $idpartida
     *
     * @return self
     */
    public function setIdpartida($idpartida)
    {
        $this->idpartida = $idpartida;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * @param mixed $orden
     *
     * @return self
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * @param mixed $origen
     *
     * @return self
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestino()
    {
        return $this->destino;
    }

    /**
     * @param mixed $destino
     *
     * @return self
     */
    public function setDestino($destino)
    {
        $this->destino = $destino;

        return $this;
    }
}