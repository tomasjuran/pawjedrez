<?php

namespace app\controllers;

use core\Controller;

class ErrorController extends Controller {
	
	public function get($request) {
		// default error is server error
		$errorHeader = "500 Internal error";
		$errorDetail = "There was an error in the server while processing your request.";

		switch ($request->url_elements[1]) {
			case "notFound":
				$errorHeader = "404 Resource not found";
				$errorDetail = "The page you requested was not found in the server.";
				break;
			case "methodNotAllowed":
				$errorHeader = "405 Method not allowed";
				$errorDetail = "You may have tried to modify a resource you have no access to.";
				break;
		} 
		require $this->view("error-page.php");
	}

}