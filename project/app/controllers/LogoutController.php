<?php

namespace app\controllers;

use core\Controller;
use core\Router;

class LogoutController extends Controller {
	
	public function post() {
		unset($_SESSION["username"]);
		Router::lastVisited();
	}
	
}