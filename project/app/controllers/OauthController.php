<?php

namespace app\controllers;

use core\Controller;
use core\App;
use core\Router;
use app\models\Usuario;

/**
 * Load Google API Library
 */
require_once '../core/oauth/google-api-php-client/vendor/autoload.php';

class OauthController extends Controller {

	public function get($request) {
		$params = $request->parameters;
		if (isset($params["error"]))
			Router::lastVisited();

		$client = new \Google_Client();
		$client->setApplicationName("PAWjedrez");
		$client->setAuthConfig(App::get("GoogleAuthConfig")["AUTH_CONFIG"]);
		$client->setRedirectUri(App::get("GoogleAuthConfig")["REDIRECT_URI"]);
		$client->setScopes(["profile", "email"]);

		// Login
		if (isset($params["code"])) {
			$client->authenticate($params["code"]);
			$access_token = $client->getAccessToken();

			$googleServiceOauth2 = new \Google_Service_Oauth2($client);
			$userInfo = $googleServiceOauth2->userinfo->get();

			if (empty($userInfo["email"])) {
				$_SESSION["errors"]["email-not-available"] = "La dirección de correo de su cuenta Google no está disponible.";
				Router::redirect("/login");
			}

			$userModel = new Usuario();
			$userModel->setEmail($userInfo["email"]);
			$select = $userModel->select();
			if (!empty($select)) {
				// Registered user
				$user = $select[0];
			} else {
				// Create new user
				$user = new Usuario();
				
				// Generate username
				$username = "";
				if (!empty($userInfo["given_name"]))
					$username .= $userInfo["given_name"];
				if (!empty($userInfo["family_name"]))
					$username .= $userInfo["family_name"];
				if (!$username)
					$username = substr($userInfo["email"], 0, strpos($userInfo["email"], "@"));
				
				$username = preg_replace("/[^a-zA-Z0-9]/", "", $username);
				$user->setNombre($username);

				if (!empty($user->select())) {
					$username = $user->getNombre() . "1";
					$user->setNombre($username);
					$i = 2;
					while (!empty($user->select())) {
						// Replace last char with a number
						$username = substr($username, 0, strlen(strval($i-1))) . strval($i);
						$user->setNombre($username);
						$i++;
					}
				}


				$user->setEmail($userInfo["email"]);
				$user->setContrasena(random_bytes(100));
				$user->insert();
			}
			
			$_SESSION["username"] = $user->getNombre();
			Router::lastVisited();
		} 

		else {
			// Ask to sign in to Google
			Router::redirect(filter_var($client->createAuthUrl(), FILTER_SANITIZE_URL));
		}
	}

}