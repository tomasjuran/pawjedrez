<?php

namespace app\controllers;

use core\Controller;
use core\Router;
use app\models\Partida;

class HomeController extends Controller {

	public function get($request) {
		if (isset($request->url_elements[1])) 
			Router::redirect("/home");
		if (isset($_SESSION["username"])) {
			$username = $_SESSION["username"];
		}
		$partidaModel = new Partida();
		$matches = $partidaModel->findNotFinishedMatches();
		require $this->view("home.php");
	}
}