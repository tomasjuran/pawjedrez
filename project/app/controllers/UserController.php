<?php

namespace app\controllers;

use core\exceptions\ResourceNotFoundException;
use core\Controller;
use app\models\{Partida, Usuario};

class UserController extends Controller {

	public function get($request) {
		$user = Usuario::findUsuarioByNombre($request->url_elements[1]);
		if (!$user)
			throw new ResourceNotFoundException("El usuario no existe.");
		$matches = Partida::findMatchByNombreUsuario($user->getNombre());
		$finished = [];
		$current = [];
		$won = 0;
		$white = 0;
		$black = 0;
		foreach ($matches as $match) {
			if ($match->getEstado() == Partida::FINISHED) {
				$finished[] = $match;
				if ($match->getBlancas()->getId() === $user->getId()) {
					$white++;
					if ($match->getGanador() == Partida::WHITE)
						$won++;
				} else {
					$black++;
					if ($match->getGanador() == Partida::BLACK)
						$won++;
				}
			}
			else
				$current[] = $match;
		}
		$played = count($finished);
		$winper = 0;
		if ($played > 0)
			$winper = $won / $played;
		require $this->view("user.php");
	}
}