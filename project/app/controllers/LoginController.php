<?php

namespace app\controllers;

use core\App;
use core\Controller;
use core\Router;
use app\models\Usuario;

class LoginController extends Controller {
	
	public function get($request) {
		if (isset($request->url_elements[1]))
			Router::redirect("/login");
		if (isset($_SESSION["username"])) {
			$_SESSION["errors"]["already-logged-in"] = "Ya has iniciado sesión.";
			Router::lastVisited();
		}
		$javascript = '
			<script type="text/javascript" src="js/login.js"></script>
		';

		if (isset($_SESSION["link-register"])) {
			unset($_SESSION["link-register"]);
			$javascript .= '
				<script>
					document.addEventListener("DOMContentLoaded", function() {
		      	tabDisplay(REGISTER);
		    	});
		    </script>
			';
		}
		require $this->view("login.php");
	}

	public function post($request) {
		$params = $request->parameters;
		foreach ($params as $paramname => $paramvalue) { 
			if (substr($paramname, 0, strlen("login-")) === "login-"
					|| substr($paramname, 0, strlen("register-")) === "register-")
				$_SESSION[$paramname] = htmlspecialchars($paramvalue);
		}
		
		if (isset($params["login"])) {
			if (!$this->verifyCredentials($params["login-username"], $params["login-password"])) {
				$_SESSION["errors"]["login"] = "El usuario y/o contraseña son incorrectos.";
				Router::redirect("/login");
			} else {
				$_SESSION["username"] = $params["login-username"];
			}
		} else 

		if (isset($params["register"])) {
			$_SESSION["link-register"] = "";
			if ($params["register-password"] != $params["register-confirm-password"]) {
				$_SESSION["errors"]["passwords-not-match"] = "Las contraseñas no coinciden.";
				$_SESSION["errors"]["register-password"] = "";
				$_SESSION["errors"]["register-confirm-password"] = "";
				Router::redirect("/login");
			}
			if (!$this->registerUser($params["register-username"], $params["register-password"], $params["register-email"])) {
				Router::redirect("/login");
			} else {
				$_SESSION["username"] = $params["register-username"];
			}
		}
		
		Router::lastVisited();
	}

	private function verifyCredentials($username, $password) {
		$user = new Usuario();
		$user->setNombre($username);
		$user->setContrasena($password);
		return $user->verifyCredentials();
	}
	
	private function registerUser($username, $password, $email) {
		try {
			$user = new Usuario();
			$user->setAll($username, $password, $email);
			$errors = $user->insert();
			if (empty($errors)) {
				return true;
			} else {
				foreach ($errors as $error => $description) {
					$_SESSION["errors"]["register-" . $error] = $description;
				}
			}
		} catch (Error $e) {
			// log error
			return false;
		}
		return false;
	}

}