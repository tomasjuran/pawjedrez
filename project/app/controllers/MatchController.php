<?php

namespace app\controllers;

use Exception;
use core\exceptions\MoveNotValidException;
use core\exceptions\ActionNotValidException;
use core\Controller;
use core\App;
use core\Router;
use app\models\Partida;
use app\models\Usuario;

class MatchController extends Controller {

	public function get($request) {
		if (!isset($request->url_elements[1]))
			Router::redirect("/match/new");

		if (isset($_SESSION["username"]))
			$username = $_SESSION["username"];
		
		// /match/new, create a new match
		if ($request->url_elements[1] == "new") {
			$this->checkUserLoggedIn();
			$match = $this->newMatch($username);
			if ($match)
				Router::redirect("/match/" . $match->getId());
			else
				Router::redirect("/home"); // Error
		} else {

			$params = $request->parameters;
			$matchId = intval($request->url_elements[1]);
			
			if (!isset($request->url_elements[2])) {
				// /match/id, match view (for playing and spectating)
				if (!$this->viewMatch($matchId))
					Router::redirect("/home");
			} 

			else {
				$action = $request->url_elements[2];
				
				if ($action == "join") {
					// /match/id/join, join to play a match
					$this->checkUserLoggedIn();
					if (!$this->joinMatch($username, $matchId))
						Router::redirect("/home");
					Router::redirect("/match/" . $matchId);
				} else 

				if ($action == "index") {
					// match/id/index/n, client current index. Send all remaining moves
					$index = 0;
					if (isset($request->url_elements[3])) {
						$index = intval($request->url_elements[3]);
					} 
					if (!isset($username))
						$username = "";
					$this->getUpdate($username, $matchId, $index);
				} 

				else
					Router::redirect("/match/".$matchId);
			} 
		}			
	}

	public function post($request) {
		if (isset($_SESSION["username"]))
			$username = $_SESSION["username"];
		else {
			echo json_encode(["error" => "Debes ingresar para realizar esta acción."]);
			exit;
		}
		if (!isset($request->url_elements[1])) {
			echo json_encode(["error" => "Se necesita un id de partida"]);
			exit;
		}

		$params = $request->parameters;
		$matchId = intval($request->url_elements[1]);

		$response = ["error" => "Hubo un error en tu solicitud."];
		$match = $this->getMatch($matchId);
		if (!$match) {
			$response["error"] = "La partida ".$matchId." no existe.";
		} else {
			if (isset($params["orig"]) && isset($params["dest"])) {
				$response = $this->makeMove($username, $match, $params);
			} else if (isset($params["concede"])) {
				$response = $this->concede($username, $match);
			} else if (isset($params["promotion"])) {
				$response = $this->promotion($username, $match, $params["promotion"]);
			}
		}
		echo json_encode($response);
	}

	// Create a new match
	private function newMatch($username) {
		$match = new Partida();
		if ($match->createMatch($username))
			return $match;

		$_SESSION["errors"]["error-creating-match"] = "Hubo un error al crear la partida.";
		return null;
	}

	// Show main match view
	private function viewMatch($matchId) {
		$match = $this->getMatch($matchId);
		if (!$match) {
			$_SESSION["errors"]["match-not-found"] = "La partida ".$matchId." no existe";
			return false;
		}
		$javascript = '
		<script src="/js/chessboard.js" type="text/javascript"></script>
		<script src="/js/chessgame.js" type="text/javascript"></script>
		<script>
	    document.addEventListener("DOMContentLoaded", function() {
	      document.getElementById("chess-board-div").appendChild(ChessGame.createBoard('.$matchId.'));
	    });
		</script>
		';
		require $this->view("match.php");
		return true;
	}

	// Join an existing match
	private function joinMatch($username, $matchId) {
		$match = $this->getMatch($matchId);
		if (!$match) {
			$_SESSION["errors"]["match-not-found"] = "La partida ".$matchId." no existe";
			return false;
		}
		if ($match->getNegras()) {
			$_SESSION["errors"]["match-full"] = "La partida ya está llena";
			return false;
		}
		if ($match->getBlancas()->getNombre() == $username) {
			Router::redirect("/match/".$matchId);
		}
		if ($match->joinMatch($username))
			return true;

		return false;
	}
	// Obtain updates from a current match (moves from $index to the last)
	private function getUpdate($username, $matchId, $index) {
		$match = $this->getMatch($matchId);
		$response = ["error" => "Hubo un error al procesar la solicitud de actualización."];
		if (!$match) {
			$response["error"] = "La partida ".$matchId." no existe.";
		} else {
			/* JSON with format
				{
					matchId: ,
					status: ,
					white: ,
					black: ,
					winner: ,
					userTurn: ,
					userColour: ,
					lastMoveId: ,
					moves: [
						{
							index: ,
							orig: ,
							dest:
						},
						{
							...
						}
					]
				}
			 */
			$response = $this->prepareMatchJSON($username, $match);
			if (!$index)
				$index = 0;
			$moves = $match->getRemainingMoves($index);
			$response["moves"] = [];
			foreach($moves as $move) {
				$response["moves"][] = [
					"index" => $move->getOrden(),
					"orig" => $move->getOrigen(),
					"dest" => $move->getDestino()
				];
			}
		}
		echo json_encode($response);
	}

	private function makeMove($username, $match, $move) {
		$orig = intval($move["orig"]);
		$dest = intval($move["dest"]);
		$response = ["error" => "Hubo un error al procesar la jugada."];
		try {
			$match->makeMove($username, $orig, $dest);
			$response = [
				"matchId" => $match->getId(),
				"move" => [
						"orig" => $orig,
						"dest" => $dest
					]
			];
		} catch (MoveNotValidException $e) {
			$response = ["error" => $e->getMessage()];
		} catch (Error $e) {
			// Log error
		}
		return $response;
	}

	private function promotion($username, $match, $promotion) {
		return ["error" => "Error"];
	}

	private function concede($username, $match) {
		$response = ["error" => "No puedes admitir derrota."];
		try {
			if ($match->concede($username)) {
				$response = $this->prepareMatchJSON($username, $match);
			}
		} catch (ActionNotValidException $e) {
			$response = ["error" => $e->getMessage()];
		} catch (Error $e) {
			// Log error
		}
		return $response;
	}

	private function getMatch($matchId) {
		if (isset($matchId)) {
			$partidaModel = new Partida();
			$partidaModel->setId($matchId);
			$select = $partidaModel->select();
			if (!empty($select)) {
				return $select[0];
			}
		}
		return null;
	}

	// Fill response with match fields
	private function prepareMatchJSON($username, $match) {
		$lastMoveId = $match->countMovimientos() - 1;
		$white = "";
		$blancas = $match->getBlancas();
		if ($blancas)
			$white = $blancas->getNombre();
		$black = "";
		$negras = $match->getNegras();
		if ($negras)
			$black = $negras->getNombre();
		$userColour = $match->getUserColour($username);
		return [
			"matchId" => $match->getId(),
			"status" => $match->getEstado(),
			"white" => $white,
			"black" => $black,
			"winner" => $match->getGanador(),
			"userTurn" => $match->getTurno(),
			"userColour" => $userColour,
			"lastMoveId" => $lastMoveId
		];
	}

}

/*
	private function searchMatch($username) {
		$response = ["error" => "Hubo un error al buscar una partida."];
		$match = new Partida();
		$userColour = null;
		// Look if user is in a match
		foreach ($match->findMatchByNombreUsuario($username) as $userMatch) {
			if ($userMatch->getEstado() != Partida::FINISHED) {
				$match = $userMatch;
				$userColour = $match->getUserColour($username);
				break;
			}
		} 

		if (!isset($userColour)) {
			// Look for an opponent without match
			foreach ($match->getPreparingMatches() as $readyMatch) {
				$whites = $readyMatch->getBlancas();
				// Don't match players with themselves
				if ($whites->getNombre() != $username) {
					// Found a match
					$match = $readyMatch;
					if ($match->joinMatch($username))
						$userColour = $match->getUserColour($username);
					break;
				}
			}
		}

		if (!isset($userColour)) {
			// Couldn't find a player without match. Create a new match.
			
		}

		$response = $this->prepareMatchJSON($match);
		$response["userColour"] = $userColour;
		return $response;
	}
*/