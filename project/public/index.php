<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require "../core/bootstrap.php";

use core\App;
use core\Router;
use core\Request;
use core\exceptions\ResourceNotFoundException;
use core\exceptions\MethodNotAllowedException;

session_start();

$logger = App::get("logger");
$request = new Request();

if (isset($_SESSION["errors"])) {
	App::bind("errors", $_SESSION["errors"]);
}
$_SESSION["errors"] = [];

try {
	Router::direct($request);
	$logger->info(logInfo("200 OK"));
} 
catch (ResourceNotFoundException $e) {
	http_response_code(404);
	Router::direct(Request::returnError("notFound"));
	$logger->error(logInfo("404 Resource not found"));
} 
catch (MethodNotAllowedException $e) {
	http_response_code(405);
	Router::direct(Request::returnError("methodNotAllowed"));
	$logger->error(logInfo("405 Method not allowed"));
} 
catch (Error $e) {
	http_response_code(500);
	Router::direct(Request::returnError("internalError"));
	$logger->fatal(logInfo("500 Internal Server Error" . PHP_EOL . $e));
}

/**
 * Information about the request
 */
function logInfo($status) {
	return $_SERVER["REMOTE_ADDR"] . ":" . $_SERVER["REMOTE_PORT"] .
			" '" . $_SERVER['REQUEST_METHOD'] . " " . $_SERVER["REQUEST_URI"] . "' " . $status;
}