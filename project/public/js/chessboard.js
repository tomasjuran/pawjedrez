"use_strict";

var ChessBoard = {

piecesDir: "/img/pieces/",
piecesMap: {
	P: "pawn",
	R: "rook",
	N: "knight",
	B: "bishop",
	Q: "queen",
	K: "king"
},
inversePiecesMap: {
	"pawn": "P",
	"rook": "R",
	"knight": "N",
	"bishop": "B",
	"queen": "Q",
	"king": "K"
},

boardId: "chess-board",

WHITE: 0,
BLACK: 1,

NROWS: 8,
NCOLS: 8,
cells: [],

Sound: function(src) {
	this.sound = document.createElement("audio");
  this.sound.src = src;
  this.sound.setAttribute("preload", "auto");
  this.sound.setAttribute("type", "audio/mp3");
  document.body.appendChild(this.sound);
  this.play = function() {
    this.sound.play();
  }
  this.stop = function() {
    this.sound.pause();
  }
},

moveSound: null,
captureSound: null,

createBoard: function(setPieces) {
	var board = document.createElement("table");

	this.moveSound = new this.Sound("/sound/Move.mp3");
	this.captureSound = new this.Sound("/sound/Capture.mp3");

	for (let i = 0; i < this.NROWS; i++) {
		let row = document.createElement("tr");
		
		for (let j = 0; j < this.NCOLS; j++) {
			var cell = document.createElement("td");
			row.appendChild(cell);
			this.cells.push(cell);
		}

		board.insertBefore(row, board.firstChild);
	}

	this.setLabels(board);
	if (setPieces) {
		this.setInitialPieces();
	}
	board.id = this.boardId;
	return board;
},

setLabels: function(board) {
	var tr = document.createElement("tr"),
			childs = board.children;

	tr.appendChild(document.createElement("th"));
	for (let char = 97; char < 105; char++) {
		var th = document.createElement("th");
		th.innerHTML = String.fromCharCode(char);
		tr.appendChild(th);
	}
	board.appendChild(tr);

	for (let i = 1; i <= this.NROWS; i++) {
		var row = childs[this.NROWS-i],
				th = document.createElement("th");
		th.innerHTML = i;
		row.insertBefore(th, row.firstChild);
	}
},

hasPiece: function(index, colour = null) {
	result = this.cells[index].firstChild;
	if (result && colour) {
		if (!this.pieceIsColour(result, colour))
			return false;
	}
	return result;
},

pieceType: function(piece) {
	var name = piece.src.substring(piece.src.lastIndexOf("/") + 1, piece.src.length - 5);
	if (name)
		return this.inversePiecesMap[name];
	return null;
},

pieceIsColour: function(piece, colour) {
	var compareChar;
	if (colour == this.WHITE) {
		compareChar = "w";
	} else if (colour == this.BLACK) {
		compareChar = "b";
	} else
		return false;

	return piece.src.charAt(piece.src.length-5) == compareChar;
},

pieceColour: function(piece) {
	if (pieceIsColour(piece, this.WHITE))
		return this.WHITE;
	else if (pieceIsColour(piece, this.BLACK))
		return this.BLACK;
	return false;
},

makeMove: function(orig, dest) {
	var pieceOrig = this.hasPiece(orig),
		typeOrig, pieceDest, move = "";
	
	if (pieceOrig) {
		typeOrig = this.pieceType(pieceOrig);
		if (typeOrig != "P")
			move = typeOrig;
		// Capture
		pieceDest = this.hasPiece(dest);
		if (pieceDest) {
			this.cells[dest].removeChild(this.cells[dest].firstChild);
			this.captureSound.play();
			move += this.posNotation(orig) + "x" + this.posNotation(dest);
		} 

		else {
			// En passant capture
			var isCapture = (dest - orig) % 7 == 0 || (dest - orig) % 9 == 0;
			if (typeOrig == "P" && isCapture) {
				if (dest > orig)
					// White move
					this.cells[dest-8].removeChild(this.cells[dest-8].firstChild);
				else 
					// Black move
					this.cells[dest+8].removeChild(this.cells[dest+8].firstChild);

				this.captureSound.play();
				move = this.posNotation(orig) + "x" + this.posNotation(dest) + "e.p.";
			}

			// Move without capture
			else {
				this.moveSound.play();
				move += this.posNotation(orig) + this.posNotation(dest);
				
				// Castling
				if (typeOrig == "K") {
					if (Math.abs(dest - orig) == 2) {
						if (dest > orig) {
							// Kingside
							this.cells[dest-1].appendChild(this.cells[dest+1].removeChild(this.cells[dest+1].firstChild));
							move = "O-O";
						}	else {
							// Queenside
							this.cells[dest+1].appendChild(this.cells[dest-2].removeChild(this.cells[dest-2].firstChild));
							move = "O-O-O";
						}
					}
				}
			}
		}
		this.cells[dest].appendChild(this.cells[orig].firstChild);
		return move;
	}
},

posNotation: function(pos) {
	var rank = (Math.floor(pos / 8) + 1).toString(),
		file = String.fromCharCode((pos % 8) + 97);
	return file + rank;
},

setPiece: function(index, type, colour) {
	var img = document.createElement("img");
	// Pawn by default
	type = type || "P";
	if (colour == this.BLACK)
		colour = "black";
	else
		colour = "white";
	img.src = this.piecesDir + this.piecesMap[type] + colour.charAt(0) + ".svg";
	img.alt = colour + " " + this.piecesMap[type];
	this.cells[index].appendChild(img);
},

setInitialPieces: function() {
	this.setFirstRow(0, 0);
	this.setPawns(8, 0);
	this.setPawns(48, 1);
	this.setFirstRow(56, 1);
},

setFirstRow: function(startIndex, colour) {
	var pieces = ["R", "N", "B", "Q", "K", "B", "N", "R"],
		j = 0;
	for (let i = startIndex; i < startIndex + 8; i++) {
		this.setPiece(i, pieces[j], colour);
		j++;
	}
},

setPawns: function(startIndex, colour) {
	for (let i = startIndex; i < startIndex + 8; i++) {
		this.setPiece(i, "", colour);
	}
}

}