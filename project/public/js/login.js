"use_strict";

const LOGIN = 0;
const REGISTER = 1;

function tabDisplay(displayId) {
	var loginTab = document.getElementById("login-tab"),
		registerTab = document.getElementById("register-tab"),
		loginForm = document.getElementById("login-form"),
		registerForm = document.getElementById("register-form"),
		TAB_ACTIVE = "tab-active";

	switch(displayId) {
		case REGISTER:
			loginTab.classList.remove(TAB_ACTIVE);
			registerTab.classList.add(TAB_ACTIVE);
			loginForm.style.display = "none";
			registerForm.style.display = "inherit";
			document.getElementById("register-username").focus();
			break;
		default:
			registerTab.classList.remove(TAB_ACTIVE);
			loginTab.classList.add(TAB_ACTIVE);
			registerForm.style.display = "none";
			loginForm.style.display = "inherit";
			document.getElementById("login-username").focus();
	}
}