"use strict";

var ChessGame = {

ChessBoard: ChessBoard,

ALERT_ID: "alert-div",
ALERT_ACTIVE: "alert-div-active",
ALERT_INACTIVE: "alert-div-inactive",
alertTimeout: null,
alertInactiveTimeout: null,

OPPONENT_ID: "match-opponent",
STATUS_ID: "match-status",
MOVES_ID: "match-moves-list",
MOVES_INDEX_CLASS: "match-moves-index",
MOVES_MOVE_CLASS: "match-moves-move",

updateInterval: null,
REFRESH_RATE: 1000,

// Game states
PREPARING: 0,
PLAYING: 1,
FINISHED: 2,

matchId: -1,
status: -1,
white: "",
black: "",
winner: -1,
userColour: -1,
userTurn: -1,
currentMoveIndex: -1,

moves: [],

selected: -1,
SELECTED_CLASS: "chess-piece-selected",
lastMoveOrig: -1,
lastMoveDest: -1,
LAST_MOVE_CLASS: "chess-piece-last-move",

createBoard: function(matchId) {
	var board = this.ChessBoard.createBoard(true);
	this.matchId = matchId;
	this.update();
	this.updateInterval = window.setInterval(function() { ChessGame.update() }, this.REFRESH_RATE);
	return board;
},

update: function() {
	var request = new XMLHttpRequest();

	// Request updates
	request.open("GET", "/match/" + this.matchId + "/index/" + this.currentMoveIndex);
	request.onload = function() {
		var update;
		if (request.status === 200 && request.responseText) {
			/* JSON with format
				{
					matchId: ,
					status: ,
					white: ,
					black: ,
					winner: ,
					userTurn: ,
					userColour: ,
					lastMoveId: ,
					moves: [
						{
							index: ,
							orig: ,
							dest:
						},
						{
							...
						}
					]
				}
			 */
			update = JSON.parse(request.responseText);
			if (parseInt(update["status"]) != ChessGame.PREPARING) {
				if (ChessGame.status < 0) 
					// Setup match
					ChessGame.startMatch(update);
				else
					// Update match info
					ChessGame.updateMatch(update);
			}
			if (update["moves"]) {
				ChessGame.updateMoves(update["moves"]);
			}
		}
	}
	request.send();
},


// Start the match when two players are online or setup to resume a match
startMatch: function(update) {
	this.matchId = update["matchId"];
	this.white = update["white"];
	this.black = update["black"];

	document.getElementById(this.OPPONENT_ID).innerHTML = 
	'<span class="match-player-white"></span> ' +
	'<a class="bold-link" href="/user/' + this.white + '">' + this.white + '</a>' +
	' vs. ' +
	'<a class="bold-link" href="/user/' + this.black + '">' + this.black + '</a>' +
	' <span class="match-player-black"></span>';

	this.userColour = update["userColour"];
	this.updateMatch(update);
	// Make cells clickable
	this.ChessBoard.cells.forEach(function(element, index) {
		element.addEventListener("click", function() {
			ChessGame.click(index);
		});
	});
},

// Select a piece or make a move
click: function(index) {
	this.removeAlert();
	if (this.status != this.PLAYING) return;

	// Cancel a move
	if (this.selected == index) {
		this.deselect();
	} else {
		if (this.selected < 0) {
			// Only select when first click is on a piece
			if (this.ChessBoard.hasPiece(index, this.userColour)) {
				this.selected = index;
				this.addCellClass(this.selected, this.SELECTED_CLASS);
			}
		} else {
			// A move was made
			this.makeMove(this.selected, index);
			this.deselect();
		}
	}
},

makeMove: function(orig, dest) {
	var request = new XMLHttpRequest(),
	move = {
		orig: orig,
		dest: dest
	};
	request.open("POST", "/match/" + this.matchId);
	request.setRequestHeader("Content-type", "application/json");
	request.onload = function() {
		var response;
		if (request.status === 200 && request.responseText) {
			response = JSON.parse(request.responseText);
			if (response["error"]) {
				ChessGame.createAlert(response["error"]);
			}		
		}
	}
	request.send(JSON.stringify(move));
	this.update();
},

// Update match related info
updateMatch: function(update) {
	if (this.userTurn != update["userTurn"] || this.status != update["status"]) {
		var statusMessage = "",
			white = '<span class="match-player-white"></span> blancas ',
			black = '<span class="match-player-black"></span> negras ',
			whitePlayer = "(" + this.white + ")",
			blackPlayer = "(" + this.black + ")";

		switch (this.userColour) {
			case ChessBoard.WHITE:
				whitePlayer = "(tú)";
				break;
			case ChessBoard.BLACK:
				blackPlayer = "(tú)";
				break;
		}

		white += whitePlayer;
		black += blackPlayer;

		this.status = update["status"];
		if (this.status == this.FINISHED) {
			// Someone has won
			this.winner = update["winner"];
			window.clearInterval(this.updateInterval);
			statusMessage = "¡Ha ganado ";
			if (this.winner == ChessBoard.BLACK)
				statusMessage += black + "!";
			else
				statusMessage += white + "!";
		} else {
			// A turn has passed
			this.userTurn = update["userTurn"];
			statusMessage = "Le toca mover a ";
			if (this.userTurn == ChessBoard.WHITE)
				statusMessage += white;
			else if (this.userTurn == ChessBoard.BLACK)
				statusMessage += black;
		}

		document.getElementById(this.STATUS_ID).innerHTML = statusMessage;	
	}
},

updateMoves: function(moves) {
	var  i = 0,
		move = moves[i];
	while (move) {
		if (move["index"] >= this.moves.length) {
			this.updateMove(move);
			this.moves.push(move);
		}
		i++;
		move = moves[i];
	} 
},

// Update board when a move was made
updateMove: function(move) {
	var orig = parseInt(move["orig"]),
		dest = parseInt(move["dest"]),
		moveList, lastMoveListItem, lastMoveListIndex, lastMoveItem,	move;

	this.deselect();
	if (this.lastMoveOrig >= 0) {
		this.removeCellClass(this.lastMoveOrig, this.LAST_MOVE_CLASS);
		this.removeCellClass(this.lastMoveDest, this.LAST_MOVE_CLASS);
	}

	this.currentMoveIndex = parseInt(move["index"]);

	move = this.ChessBoard.makeMove(orig, dest);
	moveList = document.getElementById(this.MOVES_ID);

	// ListItem full (whites turn). Create new list item
	if (this.currentMoveIndex % 2 == 0) {
		lastMoveListItem = document.createElement("li");
		
		lastMoveItem = document.createElement("p");
		lastMoveItem.classList.add(this.MOVES_INDEX_CLASS);
		lastMoveItem.innerHTML = Math.ceil((this.currentMoveIndex + 1) / 2);
		lastMoveListItem.appendChild(lastMoveItem);
		
		lastMoveItem = document.createElement("p");
		lastMoveItem.classList.add(this.MOVES_MOVE_CLASS);
		lastMoveListItem.appendChild(lastMoveItem);

		lastMoveItem = document.createElement("p");
		lastMoveItem.classList.add(this.MOVES_MOVE_CLASS);
		lastMoveListItem.appendChild(lastMoveItem);		

		moveList.appendChild(lastMoveListItem);
		lastMoveListIndex = 1;
	} else {
		lastMoveListIndex = 2;
	}

	moveList.lastElementChild.children[lastMoveListIndex].innerHTML = move;
	moveList.parentElement.scrollTop = moveList.parentElement.scrollHeight;

	this.lastMoveOrig = orig;
	this.lastMoveDest = dest;
	this.addCellClass(orig, this.LAST_MOVE_CLASS);
	this.addCellClass(dest, this.LAST_MOVE_CLASS);

},

// Promote a pawn
promotion: function() {

},

// Admit defeat
concede: function() {
	if (this.userColour != -1 && this.status == this.PLAYING) {
		var confirm = window.confirm("¿Está seguro de que desea abandonar la partida?");
		if (confirm) {
			var request = new XMLHttpRequest();
			request.open("POST", "/match/" + this.matchId);
			request.setRequestHeader("Content-type", "application/json");
			request.send(JSON.stringify({concede: true}));
		}
	}
},

createAlert: function(message) {
	var alert = document.getElementById(this.ALERT_ID);
	window.clearTimeout(this.alertTimeout);
	window.clearTimeout(this.alertInactiveTimeout);
	alert.firstChild.innerHTML = message;
	alert.classList.remove(this.ALERT_INACTIVE);
	alert.classList.add(this.ALERT_ACTIVE);
	this.alertTimeout = window.setTimeout(function() { ChessGame.removeAlert() }, 5000);
},

removeAlert: function() {
	var alert = document.getElementById(this.ALERT_ID);
	if (alert.classList.contains(this.ALERT_ACTIVE)) {
		window.clearTimeout(this.alertTimeout);
		window.clearTimeout(this.alertInactiveTimeout);
		alert.classList.remove(this.ALERT_ACTIVE);
		this.alertInactiveTimeout = window.setTimeout(function () {
			alert.classList.add(ChessGame.ALERT_INACTIVE);
		}, 1000);
	}
},

// Clear selection
deselect: function() {
	if (this.selected != -1) {
		this.removeCellClass(this.selected, this.SELECTED_CLASS);
		this.selected = -1;
	}
},

addCellClass: function(index, cssClass) {
	this.ChessBoard.cells[index].classList.add(cssClass);
},

removeCellClass: function(index, cssClass) {
	this.ChessBoard.cells[index].classList.remove(cssClass);
},


}