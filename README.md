# PAWjedrez

Final project for "Programación en Ambiente Web" course in Universidad Nacional de Luján.

Play chess online with a friend!

# Install

La carpeta del proyecto es la carpeta principal con la configuración y recursos del sitio. Se realizará la instalación en el directorio */home/user*, es decir, la carpeta de proyecto será */home/user/project*. En caso de omitir la ruta completa, se asume que se parte del directorio *project*.

## Configuración inicial

Para correr el sitio web, se requiere del stack LAMP (Linux, Apache, MySQL, PHP). Para Ubuntu, instalar con:

`$ sudo apt install apache2 mysql-server php libapache2-mod-php php-mysql`

Es necesario tener habilitado el módulo Mod_Rewrite de Apache para el archivo *public/.htaccess*.

`$ sudo a2enmod rewrite`

Reiniciar Apache.

`$ sudo systemctl restart apache2`

Crear el archivo para loguear las solicitudes del servidor y darle permisos a Apache:

`$ touch /home/user/project/server.log`

`$ sudo chown www-data:www-data server.log`

## Configurar la Base de Datos MySQL

Correr en un cliente MySQL los scripts que se encuentran en la carpeta *sql* en orden (v0.0.1 primero, luego v0.0.2, y así sucesivamente).

Crear la configuración del archivo *config.php* utilizando como ejemplo el archivo *config.php.example*.

`$ cp config.php.example config.php`

* *DBNAME* es el nombre del esquema de la BD; según el script que crea el esquema, *paw.tpi*
* *USERNAME* es el nombre del usuario de la BD
* *PASSWORD* es la contraseña de acceso
* *CONNECTION* es la URL de conexión a la BD

## Configuración del servidor

**Para la configuración rápida con el built-in server de PHP, continuar leyendo la siguiente sección "Configuración rápida PHP Server". Si no se quiere utilizar este método, saltar a la sección "Configuración de Apache VirtualHost".**

## Configuración rápida PHP built-in server

Para utilizar el PHP built-in server, simplemente ubicarse en el directorio *public* y correr el servidor.

`$ cd /home/user/project/public`

`$ php -S localhost:8000`

Si todo salió bien, al ingresar *localhost:8000* en la barra de direcciones del buscador, se debería mostrar la página de inicio.

**Saltar a la sección "Configurar el Cliente PHP para la API de Google para Oauth2".**

## Configuración de Apache VirtualHost

Para esta configuración, es necesario crear un archivo Virtual Host e indicar como Document Root la carpeta *public*.

Se creará el archivo de Virtual Host en */etc/apache2/sites-available/*.

`$ sudo touch /etc/apache2/sites-available/pawjedrez.conf`

Luego, abrir con un editor de texto el archivo recién creado y copiar y pegar lo siguiente:

```
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	ServerName www.pawjedrez.com
	DocumentRoot /home/user/project/public

	<Directory /home/user/project/public/>
		Options FollowSymLinks
		AllowOverride All
		Require all granted
	</Directory>

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Activar el sitio creado.

`$ sudo a2ensite pawjedrez.conf`

Reiniciar Apache.

`$ sudo systemctl restart apache2`

Si todo salió bien, al ingresar *www.pawjedrez.com* en la barra de direcciones del buscador, se debería mostrar la página de inicio.

## Configurar el Cliente PHP para la API de Google para OAuth2

En el archivo de configuración *config.php*, agregar:

* *AUTH_CONFIG* archivo con las credenciales para la Google API en formato json
* *REDIRECT_URI* dirección que se utiliza para acceder al servidor, por ejemplo *http://www.pawjedrez.com* o *http://localhost:8000*

Descargar la librería del Cliente PHP para la API de Google y ubicarla en el directorio *core/oauth/*.

`$ wget https://github.com/googleapis/google-api-php-client/releases/download/v2.2.2/google-api-php-client-2.2.2.zip`

`$ unzip google-api-php-client-2.2.2.zip`

`$ mv google-api-php-client-2.2.2 core/oauth/google-api-php-client`

Si es necesario, agregar la dirección que se utiliza para acceder al sitio en el archivo de hosts para redireccionar correctamente. Por ejemplo, para *www.pawjedrez.com*:

`$ sudo sed -i "1 a 127.0.0.1    www.pawjedrez.com" /etc/hosts`
